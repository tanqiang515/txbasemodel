package cn.com.tx.core.base.model;

/**
 * @ClassName IBaseUserModel
 * @描述 TODO
 * @Author 谭强
 * @Date 2018-08-15 16:34
 * @Version 1.0
 * @最后修改时间 TODD
 * @修改记录
 **/
public interface IBaseUserModel {

    public Long getId();

    public String getUsername();

    public String getPassword();


    public String getMobile();

    public Integer getStatus();
}
