package cn.com.tx.core.base.model;

import java.util.Date;

/**
 * @ClassName IBaseTokenModel
 * @描述 TODO
 * @Author 谭强
 * @Date 2018-08-15 16:30
 * @Version 1.0
 * @最后修改时间 TODD
 * @修改记录
 **/
public interface IBaseTokenModel {

    public Long getUserId();

    public String getToken();


    public Date getExpireTime();

    public Date getUpdateTime();
}
