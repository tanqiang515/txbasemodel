package cn.com.tx.core.base.model;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName UserInfoEntity
 * @描述 TODO
 * @Author 谭强
 * @Date 2018-08-10 10:34
 * @Version 1.0
 * @最后修改时间 TODD
 * @修改记录
 **/
public class BaseUserModel implements Serializable,IBaseUserModel {
    private static final long serialVersionUID = 1L;

    /**用户ID*/
    private Long id;

    /** 用户名 */
    private String username;

    /**密码*/
    private String password;

    /**手机号*/
    private String mobile;

    /**状态  0：禁用   1：正常*/
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
