package cn.com.tx.core.base.model;

/**
 * @ClassName TxUserContext
 * @描述 TODO
 * @Author 谭强
 * @Date 2018-08-21 11:20
 * @Version 1.0
 * @最后修改时间 TODD
 * @修改记录
 **/

public class TxUserContext {

    private String token;

    private IBaseUserModel userInfo;



    public IBaseUserModel getUserInfo(){
        return userInfo;
    }

    public void setUserInfo(IBaseUserModel userInfo){
        this.userInfo = userInfo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
